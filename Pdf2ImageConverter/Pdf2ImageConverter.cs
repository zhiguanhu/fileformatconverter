﻿using Alphaleonis.Win32.Filesystem;
using ImageMagick;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Pdf2ImageConverter
{
    public class Pdf2ImageConverter
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        public static bool Pdf2Image(String inputFilePath, String outputPath,
            int resizeHeight = 256, int resizeWidth = 256, int maxPageNumber = int.MaxValue, string extension = ".jpg",
            double FixWidthRatio = 3)
        {
            //If output path already exists, means the file has already been processed. Otherwise, will only have temp output path.
            if (Directory.Exists(outputPath))
            {
                logger.Info("Output folder already exist! Skip file {0}...", Path.GetFileName(inputFilePath));
                return true;
            }
            var outputTempPath = outputPath + "_temp";

            if (!Directory.Exists(outputTempPath))
                Directory.CreateDirectory(outputTempPath);

            //int pageCount = 0;
            MagickReadSettings settings = new MagickReadSettings();
            //limit thread number to 1.
            //ImageMagick.ResourceLimits.Thread = 2;

            //settings.Density = new Density(400);
            settings.FrameIndex = 0;
            settings.FrameCount = maxPageNumber;
            ImageMagick.ResourceLimits.Thread = 1;

            int pageNumber = 0;
            var inputFileInfo = new FileInfo(inputFilePath);

            if (!inputFileInfo.Exists)
            {
                return false;
            }
            MagickGeometry size = new MagickGeometry(resizeHeight, resizeWidth);
            size.IgnoreAspectRatio = true;

            using (MagickImageCollection collection = new MagickImageCollection())
            {

                collection.Read(inputFilePath, settings);

                foreach(MagickImage image in collection)
                {
                    //Try to fill transparent background with white, not work with the following line.
                    //image.BackgroundColor = new MagickColor(System.Drawing.Color.White);
                    image.ColorAlpha(System.Drawing.Color.White);

                    //if height or width are extremly large, will have one of the length fixed.
                    if ((double)image.Height / image.Width > FixWidthRatio)
                    {
                        image.Resize(resizeWidth, 0);
                        image.Crop(0,0,resizeWidth, (int)Math.Floor(resizeWidth * FixWidthRatio));
                    }
                    else if ((double)image.Width / image.Height > FixWidthRatio)
                    {
                        image.Resize(0, resizeHeight);
                        image.Crop(0,0,(int)Math.Floor(resizeHeight * FixWidthRatio), resizeHeight);
                    }
                    else
                    {
                        image.Resize(size);
                    }

                    //fill in white background for transparent ones.
                    //using (var bm = image.ToBitmap())
                    //{
                    //    bm.Save(Path.Combine(outputTempPath, Path.GetFileNameWithoutExtension(inputFilePath) + "." + pageNumber + ".jpg"));
                    //}
                    image.Write(Path.Combine(outputTempPath, Path.GetFileNameWithoutExtension(inputFilePath) + "." + pageNumber + ".jpg"));
                    pageNumber++;
                }
            }

            logger.Info(Path.GetFileNameWithoutExtension(inputFilePath) + ": " + pageNumber);

            //create final folder.
            bool renameSuccess = false;
            int tryCount = 10;
            while (!renameSuccess)
            {
                try
                {
                    Directory.Move(outputTempPath, outputPath);
                    renameSuccess = true;
                }
                catch (Exception e)
                {
                    System.Threading.Thread.Sleep(1000);
                }
                finally
                {
                    tryCount--;
                }

                if (tryCount == 0)
                {
                    logger.Error("Failed to rename the folder: {0}", inputFilePath);
                    return false;
                }
            }

            return true;
        }
    }
}

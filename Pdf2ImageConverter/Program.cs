﻿using Alphaleonis.Win32.Filesystem;
using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;


namespace Pdf2ImageConverter
{
    class Program
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Pdf2ImageOptions>(args)
                .WithParsed<Pdf2ImageOptions>(opts => ConvertPdf2Image(opts));
                //.WithNotParsed<Pdf2ImageOptions>((errs) => HandleParseError(errs));

            //var inputFilePath = @"\\gaia\C$\Users\zhu\Documents\Visual Studio 2015\Projects\FileFormatConverter\Pdf2ImageConverterTests\TestData\1110967.pdf";
            //var outputPath = @"\\gaia\C$\Users\zhu\Documents\Visual Studio 2015\Projects\FileFormatConverter\Pdf2ImageConverterTests\TestData\1110967";
            ////var inputFilePath = args[0];
            ////var outputPath = args[1];
            //Pdf2ImageConverter.Pdf2Image(inputFilePath, outputPath, maxPageNumber:3, FixWidthRatio:1);

            
        }
        public static void ConvertPdf2Image(Pdf2ImageOptions opts)
        {
            if(opts.ProcessDirectory)
            {
                string[] files;
                if (opts.SearchAllSubDirs)
                {
                    files = Directory.GetFiles(opts.InputPath, opts.ProcessFileTypeFilter, System.IO.SearchOption.AllDirectories);
                }else
                {
                    files = Directory.GetFiles(opts.InputPath, opts.ProcessFileTypeFilter, System.IO.SearchOption.TopDirectoryOnly);
                }
                logger.Info("File count: {0}", files.Length);

                int processedFileCount = 0;
                object fileCountLock = new object();

                Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = opts.ThreadNumber }, file =>
                 {
                     var outputFileFolder = Path.Combine(opts.OutputPath, Path.GetFileNameWithoutExtension(file));
                     
                     ConvertSingleFile(file, outputFileFolder, opts);
                     lock(fileCountLock)
                     {
                         processedFileCount++;
                         if(processedFileCount % 100 == 0)
                         {
                             logger.Info("Processed: {0}", processedFileCount);
                         }
                     }
                 });
            }else
            {
                var outputFileFolder = Path.Combine(opts.OutputPath, Path.GetFileNameWithoutExtension(opts.InputPath));
                ConvertSingleFile(opts.InputPath, outputFileFolder, opts);
            }
        }

        public static void ConvertSingleFile(string inputFilePath, string outputFilePath, Pdf2ImageOptions opts)
        {
            try
            {
                Pdf2ImageConverter.Pdf2Image(inputFilePath, outputFilePath, opts.ResizeHeight,
                    opts.ResizeWidth, opts.MaxConvertPageNumber, opts.ImageExtension, opts.FixOneSideRatio);
            }catch(Exception e)
            {
                logger.Error("File: {0}", inputFilePath);
                logger.Error(e.ToString());
            }
        }

        public static void HangeParseError(Error errs)
        {
            Console.WriteLine(errs.Tag);
        }
    }

    //String inputFilePath, String outputPath,
    //int resizeHeight = 256, int resizeWidth = 256, int maxPageNumber = int.MaxValue, string extension = ".jpg",
    //double FixWidthRatio = 3
    class Pdf2ImageOptions
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        [Option('i', "input", Required = true, HelpText ="Input file/path")]
        public string InputPath { get; set; }
        [Option('o', "output", Required = true, HelpText ="Output file path.")]
        public string OutputPath { get; set; }
        [Option('e', "extension", Default =".jpg", HelpText ="Image file extension")]
        public string ImageExtension { get; set; }
        [Option('d', "process_directory", Default = false, HelpText = "True if input is a directory. False for a single file.")]
        public bool ProcessDirectory { get; set; }
        [Option('f', "input_file_filters", Default = "*.pdf", HelpText = "Pass file filter for Directory.GetFiles. E.g. *.pdf")]
        public string ProcessFileTypeFilter { get; set; }
        [Option('h', "resize_height", Default = 256, HelpText = "image height after resizing")]
        public int ResizeHeight { get; set; }
        [Option('w', "resize_width", Default = 256, HelpText = "image width after resizing")]
        public int ResizeWidth { get; set; }
        [Option('n', "page_number", Default = 3, HelpText = "number of pages to be extracted")]
        public int MaxConvertPageNumber { get; set; }
        [Option('r', "maxWidthHeightRatio", Default = 3.0, HelpText = "if width/height or height/width is larger than this ratio, crop the image.")]
        public double FixOneSideRatio { get; set; }
        [Option('t', "thread_number", Default = 1, HelpText = "Thread number when processing a directory.")]
        public int ThreadNumber { get; set; }
        [Option('s', "search_all_sub_dirs", Default = true, HelpText = "Search all subdirectories.")]
        public bool SearchAllSubDirs { get; set; }
    }
}

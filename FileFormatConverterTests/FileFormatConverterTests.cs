﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileFormatConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileFormatConverter.Tests
{
    [TestClass()]
    public class FileFormatConverterTests
    {
        [TestMethod()]
        public void ConvertDoc2Pdf_SingleTest()
        {
            //var inputDoc = @"\\maia\h$\DataHelix_QUEUE\TO_PROCESS\MRO\00002\8256\109E19772D1609C895B1282AA22D8560.doc";
            //var outputPdf = @"\\maia\h$\DataHelix_QUEUE\PROCESSING\MRO\00002\8256\doc2pdf\109E19772D1609C895B1282AA22D8560.doc.pdf";
            //var inputDoc = Path.Combine(Environment.CurrentDirectory, @"..\..\TestData\docs\Test3.doc");
            //var outputPdf = Path.Combine(Environment.CurrentDirectory, @"..\..\TestData\doc2pdf\Test3.doc.pdf");
            var inputDoc = @"\\maia\h$\DataHelix_QUEUE\TO_PROCESS\UL\00005\-2\106D8BA727C88A6BA7E7842B27FDE31D.docx";
            var outputPdf = @"C:\Users\zhu\Documents\Workspace\UL_Units\bookmark_test\106D8BA727C88A6BA7E7842B27FDE31D.docx.pdf";

            if (File.Exists(outputPdf))
            {
                File.Delete(outputPdf);
            }

            FileFormatConverter.ConvertDoc2Pdf_Single(inputDoc, outputPdf);

            Assert.IsTrue(System.IO.File.Exists(outputPdf));
        }

        [TestMethod()]
        public void ConvertDoc2Pdf_BatchTest()
        {
            //var inputFolder = @"\\maia\h$\DataHelix_QUEUE\TO_PROCESS\MRO\00002\8256\";
            //var outputFolder = @"\\maia\h$\DataHelix_QUEUE\PROCESSING\MRO\00002\8256\doc2pdf";
            var inputFolder = Path.Combine(Environment.CurrentDirectory, @"..\..\TestData\docs");
            var outputFolder = Path.Combine(Environment.CurrentDirectory, @"..\..\TestData\doc2pdf");

            var oldPdfFiles = Directory.GetFiles(outputFolder, @"*.pdf");

            if (oldPdfFiles.Length > 0)
            {
                oldPdfFiles.ToList().ForEach(x => File.Delete(x));
            }

            FileFormatConverter.ConvertDoc2Pdf_Batch(inputFolder, outputFolder);

            var docFiles = Directory.GetFiles(inputFolder, @"*.doc").ToList();
            //docFiles.AddRange(Directory.GetFiles(inputFolder, @"*.docx"));

            var pdfFiles = Directory.GetFiles(outputFolder, @"*.pdf").ToList();

            Assert.AreEqual(docFiles.Count, pdfFiles.Count);
        }

        [TestMethod()]
        public void ConvertDoc2Docx_SingleTest()
        {
            var testFilePath = @"\\maia\h$\DataHelix_QUEUE\PROCESSING\MRO\00003\2126\testDoc\13C93726C1A6C1F125C2881F73EEF803.doc";
            var testOutputFile = @"\\maia\h$\DataHelix_QUEUE\PROCESSING\MRO\00003\2126\testDoc\13C93726C1A6C1F125C2881F73EEF803.docx";
            FileFormatConverter.ConvertDoc2Docx_Single(testFilePath, testOutputFile);
        }

        [TestMethod()]
        public void ConvertXlsx2Pdf_SingleFileTest()
        {
            var testFilePath = @"\\maia\h$\DataHelix_QUEUE\TO_PROCESS\NFX\00045\1922\1C757B1F7781AAF55AE87E212F7C3548.xlsx";
            var testOutput = @"\\maia\h$\DataHelix_QUEUE\PROCESSING\NFX\00045\1922\xls2pdf";
            FileFormatConverter.ConvertXlsx2Pdf_SingleFile_BackgroundWorker(testOutput, testFilePath);
        }
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pdf2ImageConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdf2ImageConverter.Tests
{
    [TestClass()]
    public class Pdf2ImageConverterTests
    {
        [TestMethod()]
        public void Pdf2BmpTest()
        {
            var inputFilePath = @"\\gaia\C$\Users\zhu\Documents\Visual Studio 2015\Projects\FileFormatConverter\Pdf2ImageConverterTests\TestData\1110967.pdf";
            var outputPath = @"\\gaia\C$\Users\zhu\Documents\Visual Studio 2015\Projects\FileFormatConverter\Pdf2ImageConverterTests\TestData\1110967";
            Pdf2ImageConverter.Pdf2Image(inputFilePath, outputPath, 3);
        }
    }
}
﻿using Alphaleonis.Win32.Filesystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.ComponentModel;

namespace FileFormatConverter
{
    public class FileFormatConverter
    {
        /// <summary>
        /// Convert ppt/pptx files to pdfs in the same folder.
        /// </summary>
        /// <param name="filesFolder"></param>
        /// <param name="outputFolder"></param>
        public static void ConvertPPT2Pptx_Batch(string filesFolder, string outputFolder)
        {
            var files = Directory.GetFiles(filesFolder).Where(x => x.ToLower().EndsWith("ppt"));
            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

            //Not sure if word can process more than 1 file at the same time without create new application.
            Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 1 }, file =>
            {
                var outputFile = Path.Combine(outputFolder, Path.GetFileName(file) + ".pptx");
                if (File.Exists(outputFile))
                    return;

                ConvertPPT2Pptx(file, outputFile);
            });
        }
        /// <summary>
        /// Convert ppt/pptx files to pdfs in the same folder.
        /// </summary>
        /// <param name="filesFolder"></param>
        /// <param name="outputFolder"></param>
        public static void ConvertPPT2Pdf_Batch(string filesFolder, string outputFolder)
        {
            var files = Directory.GetFiles(filesFolder).Where(x => x.ToLower().EndsWith("ppt") || x.ToLower().EndsWith("pptx"));
            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

            //Not sure if word can process more than 1 file at the same time without create new application.
            Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 1 }, file =>
            {
                var outputFile = Path.Combine(outputFolder, Path.GetFileName(file) + ".pdf");
                if (File.Exists(outputFile))
                    return;

                ConvertPPT2Pdf(file, outputFile);
            });
        }
        public static void ConvertPPT2Pptx(String inputFile, String outputFile)
        {
            Microsoft.Office.Interop.PowerPoint.Application pptApplication = null;
            Microsoft.Office.Interop.PowerPoint.Presentation pptPresentation = null;
            try
            {
                object unknownType = Type.Missing;

                //start power point
                pptApplication = new Microsoft.Office.Interop.PowerPoint.Application();

                //open powerpoint document
                pptPresentation = pptApplication.Presentations.Open(inputFile,
                    Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoTrue,
                    Microsoft.Office.Core.MsoTriState.msoFalse);

                // save PowerPoint as PPTX
                pptPresentation.SaveCopyAs(outputFile, Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLPresentation);
            }
            finally
            {
                // Close and release the Document object.
                if (pptPresentation != null)
                {
                    pptPresentation.Close();
                    pptPresentation = null;
                }

                // Quit PowerPoint and release the ApplicationClass object.
                if (pptApplication != null)
                {
                    pptApplication.Quit();
                    pptApplication = null;
                }
            }
        }
        public static void ConvertPPT2Pdf(String inputFile, String outputFile)
        {
            Microsoft.Office.Interop.PowerPoint.Application pptApplication = null;
            Microsoft.Office.Interop.PowerPoint.Presentation pptPresentation = null;
            try
            {
                object unknownType = Type.Missing;

                //start power point
                pptApplication = new Microsoft.Office.Interop.PowerPoint.Application();

                //open powerpoint document
                pptPresentation = pptApplication.Presentations.Open(inputFile,
                    Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoTrue,
                    Microsoft.Office.Core.MsoTriState.msoFalse);

                // save PowerPoint as PDF
                pptPresentation.ExportAsFixedFormat(outputFile,
                    Microsoft.Office.Interop.PowerPoint.PpFixedFormatType.ppFixedFormatTypePDF,
                    Microsoft.Office.Interop.PowerPoint.PpFixedFormatIntent.ppFixedFormatIntentPrint,
                    Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Interop.PowerPoint.PpPrintHandoutOrder.ppPrintHandoutVerticalFirst,
                    Microsoft.Office.Interop.PowerPoint.PpPrintOutputType.ppPrintOutputSlides, Microsoft.Office.Core.MsoTriState.msoFalse, null,
                    Microsoft.Office.Interop.PowerPoint.PpPrintRangeType.ppPrintAll, string.Empty, true, true, true,
                    true, false, unknownType);
            }
            finally
            {
                // Close and release the Document object.
                if (pptPresentation != null)
                {
                    pptPresentation.Close();
                    pptPresentation = null;
                }

                // Quit PowerPoint and release the ApplicationClass object.
                if (pptApplication != null)
                {
                    pptApplication.Quit();
                    pptApplication = null;
                }
            }
        }
        /// <summary>
        /// Convert doc&docx files to pdfs in the same folder.
        /// </summary>
        /// <param name="filesFolder"></param>
        /// <param name="outputFolder"></param>
        public static void ConvertDoc2Pdf_Batch(String filesFolder, String outputFolder)
        {
            //-->Prepare word
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();

            // Use the dummy value as a placeholder for optional arguments
            object oMissing = System.Reflection.Missing.Value;
            object fileFormat = WdSaveFormat.wdFormatPDF;

            word.Visible = false;
            word.ScreenUpdating = false;
            //<--End of prepare word
            
            var files = Directory.GetFiles(filesFolder).Where(x => x.ToLower().EndsWith("doc") || x.ToLower().EndsWith("docx") || x.ToLower().EndsWith("rtf"));
            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);
            
            //Not sure if word can process more than 1 file at the same time without create new application.
            Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 1 }, file =>
            {
                var outputFile = Path.Combine(outputFolder, Path.GetFileName(file) + ".pdf");
                if (File.Exists(outputFile))
                    return;

                ConvertDoc2Pdf(word, ref oMissing, file, outputFile, ref fileFormat);

            });

            ((_Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;
        }

        /// <summary>
        /// Convert a single doc/docx file to a pdf file.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        public static void ConvertDoc2Pdf_Single(String inputFile, String outputFile)
        {
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();

            // Use the dummy value as a placeholder for optional arguments
            object oMissing = System.Reflection.Missing.Value;
            object fileFormat = WdSaveFormat.wdFormatPDF;

            word.Visible = false;
            word.ScreenUpdating = false;

            ConvertDoc2Pdf(word, ref oMissing, inputFile, outputFile, ref fileFormat);

            ((_Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;
        }

        /// <summary>
        /// Convert a single doc/docx file to a pdf file.
        /// </summary>
        /// <param name="word"></param>
        /// <param name="oMissing"></param>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        /// <param name="fileFormat"></param>
        private static void ConvertDoc2Pdf(Application word, ref object oMissing, string inputFile, string outputFile, ref object fileFormat)
        {
            object fileName = (object)inputFile;
            object outputFileName = (object)outputFile;

            Document doc = word.Documents.Open(ref fileName, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            doc.Activate();

            // Save document into PDF Format
            doc.SaveAs(ref outputFileName,
                ref fileFormat, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            // Close the Word document, but leave the Word application open.
            // doc has to be cast to type _Document so that it will find the
            // correct Close method.                
            object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
            ((_Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
            doc = null;
        }

        /// <summary>
        /// Convert Excel format from .xls/.csv/.xlt to .xlsx.
        /// </summary>
        /// <param name="filesFolder"></param>
        /// <param name="outputFolder"></param>
        public static void ConvertXls2Xlsx_Batch(String filesFolder, String outputFolder)
        {
            var files = Directory.GetFiles(filesFolder).Where(x => x.ToLower().EndsWith("xls") || x.ToLower().EndsWith("csv") || x.ToLower().EndsWith("xlt"));
            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

            //foreach (var file in files) {
            Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 4 }, file =>
            {
                ConvertXls2Xlsx_SingleFile(outputFolder, file);
            });
        }

        /// <summary>
        /// Convert Excel format from .xls/.csv/.xlt to .xlsx.
        /// </summary>
        /// <param name="outputFolder"></param>
        /// <param name="file"></param>
        private static void ConvertXls2Xlsx_SingleFile(string outputFolder, string file)
        {
            //if (File.Exists(Path.Combine(outputFolder, Path.GetFileNameWithoutExtension(file) + ".xlsx"))
            //                || File.Exists(Path.Combine(outputFolder, Path.GetFileNameWithoutExtension(file) + ".xlsm")))
            //    return;
            if (File.Exists(Path.Combine(outputFolder, Path.GetFileName(file) + ".xlsx"))
                            || File.Exists(Path.Combine(outputFolder, Path.GetFileName(file) + ".xlsm")))
                return;

            var app = new Microsoft.Office.Interop.Excel.Application();
            app.DisplayAlerts = false;

            //try to remove macro and error displays
            app.EnableEvents = false;
            try
            {
                var wb = app.Workbooks.Open(file);
                //not going to enable macro which will cause security problems.
                wb.SaveAs(Filename: Path.Combine(outputFolder, Path.GetFileName(file) + ".xlsx"), FileFormat: Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook);
                wb.Close();
                app.Quit();
                Console.WriteLine("Finish: " + Path.GetFileName(file));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                Console.WriteLine(file);
            }
            finally
            {
                //NOTE: Sometimes it will kill all excel processes in the same session.
                try
                {
                    if (app != null && app.Workbooks != null && app.Workbooks.Count < 2)
                    {
                        app.DisplayAlerts = false;
                        app.Quit();
                        Kill(app.Hwnd);
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(app);
                        app = null;
                    }
                }
                catch
                {
                    /// Excel may have been closed via Windows Task Manager.
                    /// Skip the close.
                }
            }
        }
        /// <summary>
        /// Convert Excel format from .xls/.csv/.xlt to .xlsx.
        /// </summary>
        /// <param name="filesFolder"></param>
        /// <param name="outputFolder"></param>
        public static void ConvertXlsX2Pdf_Batch(String filesFolder, String outputFolder, int threadCount=2)
        {
            var files = Directory.GetFiles(filesFolder).Where(x => x.ToLower().EndsWith("xls") || x.ToLower().EndsWith("csv") 
            || x.ToLower().EndsWith("xlt") || x.ToLower().EndsWith("xlsx") || x.ToLower().EndsWith("xlsm"));
            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

            //foreach (var file in files) {
            Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = threadCount }, file =>
            {
                ConvertXlsx2Pdf_SingleFile(outputFolder, file);
            });
        }
        /// <summary>
        /// try use backgroundworker to suppress progress dialog.
        /// </summary>
        /// <param name="outputFolder"></param>
        /// <param name="file"></param>
        public static void ConvertXlsx2Pdf_SingleFile_BackgroundWorker(string outputFolder, string file)
        {
            var worker = new BackgroundWorker();
            worker.DoWork += (o, args) =>
            {
                ConvertXlsx2Pdf_SingleFile(outputFolder, file);
            };
            worker.RunWorkerAsync();
        }
        /// <summary>
        /// Convert Excel format from .xls/.csv/.xlt to .pdf.
        /// </summary>
        /// <param name="outputFolder"></param>
        /// <param name="file"></param>
        public static void ConvertXlsx2Pdf_SingleFile(string outputFolder, string file)
        {
            //var outputFileName = Path.Combine(outputFolder, Path.GetFileNameWithoutExtension(file) + ".pdf");
            //Should be better is we keep the original extensions.
            var outputFileName = Path.Combine(outputFolder, Path.GetFileName(file) + ".pdf");
            if (File.Exists(outputFileName))
                return;

            var app = new Microsoft.Office.Interop.Excel.Application();
            app.DisplayAlerts = false;
            try
            {
                var wb = app.Workbooks.Open(file);
                //wb.SaveAs(Filename: Path.Combine(outputFolder, Path.GetFileNameWithoutExtension(file) + ".pdf"), FileFormat: Microsoft.Office.Interop.Excel.XlFileFormat.pdf);
                wb.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, outputFileName, null, null, false);
                wb.Close();
                app.Quit();
                Console.WriteLine("Finish: " + Path.GetFileName(file));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message + "\n" + e.StackTrace);
                Console.WriteLine(file);
            }
            finally
            {
                //NOTE: Sometimes it will kill all excel processes in the same session.
                try
                {
                    if (app != null && app.Workbooks != null && app.Workbooks.Count < 2)
                    {
                        app.DisplayAlerts = false;
                        app.Quit();
                        Kill(app.Hwnd);
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(app);
                        app = null;
                    }
                }
                catch
                {
                    /// Excel may have been closed via Windows Task Manager.
                    /// Skip the close.
                }
            }
        }
        /// <summary>
        /// Try to kill Excel processes when it is not exit after running.
        /// </summary>
        /// <param name="hwnd"></param>
        private static void Kill(int hwnd)
        {
            int excelPID = 0;
            int handle = hwnd;
            GetWindowThreadProcessId(handle, ref excelPID);

            Process process = null;
            try
            {
                process = Process.GetProcessById(excelPID);

                //
                // If we found a matching Excel proceess with no main window
                // associated main window, kill it.
                //
                if (process != null)
                {
                    if (process.ProcessName.ToUpper() == "EXCEL" && !process.HasExited)
                        process.Kill();
                }
            }
            catch { }
        }

        [DllImport("user32")]
        public static extern int GetWindowThreadProcessId(int hwnd, ref int lpdwProcessId);

        /// <summary>
        /// Convert doc&docx files to pdfs in the same folder.
        /// </summary>
        /// <param name="filesFolder"></param>
        /// <param name="outputFolder"></param>
        public static void ConvertDoc2Docx_Batch(String filesFolder, String outputFolder)
        {
            //-->Prepare word
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();

            // Use the dummy value as a placeholder for optional arguments
            object oMissing = System.Reflection.Missing.Value;
            object fileFormat = WdSaveFormat.wdFormatXMLDocument;

            word.Visible = false;
            word.ScreenUpdating = false;
            //<--End of prepare word

            var files = Directory.GetFiles(filesFolder).Where(x => x.ToLower().EndsWith("doc") || x.ToLower().EndsWith("rtf"));// && Path.GetFileName(x).StartsWith("88"));
            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

            //Not sure if word can process more than 1 file at the same time without create new application.
            Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 1 }, file =>
            {
                var outputFile = Path.Combine(outputFolder, Path.GetFileName(file) + ".docx");
                if (File.Exists(outputFile))
                    return;

                ConvertDoc2Docx(word, ref oMissing, file, outputFile, ref fileFormat);

            });

            ((_Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;
        }

        /// <summary>
        /// Convert a single doc/docx file to a pdf file.
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        public static void ConvertDoc2Docx_Single(String inputFile, String outputFile)
        {
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();

            // Use the dummy value as a placeholder for optional arguments
            object oMissing = System.Reflection.Missing.Value;
            object fileFormat = WdSaveFormat.wdFormatXMLDocument;

            word.Visible = false;
            word.ScreenUpdating = false;

            ConvertDoc2Docx(word, ref oMissing, inputFile, outputFile, ref fileFormat);

            ((_Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;
        }

        /// <summary>
        /// Convert a single doc/docx file to a pdf file.
        /// </summary>
        /// <param name="word"></param>
        /// <param name="oMissing"></param>
        /// <param name="inputFile"></param>
        /// <param name="outputFile"></param>
        /// <param name="fileFormat"></param>
        private static void ConvertDoc2Docx(Application word, ref object oMissing, string inputFile, string outputFile, ref object fileFormat)
        {
            object fileName = (object)inputFile;
            object outputFileName = (object)outputFile;

            Document doc = word.Documents.Open(ref fileName, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing);
            doc.Activate();
            doc.Convert();

            // Save document into PDF Format
            doc.SaveAs(ref outputFileName,
                ref fileFormat, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            // Close the Word document, but leave the Word application open.
            // doc has to be cast to type _Document so that it will find the
            // correct Close method.                
            object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
            ((_Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
            doc = null;
        }
    }
}

﻿using Alphaleonis.Win32.Filesystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using NLog;

namespace FileFormatConverter
{
    class Program
    {
        //set up logger
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        static string queuePath = @"\\maia\h$\DataHelix_QUEUE\TO_PROCESS";
        static string queueProcessingPath = @"\\maia\h$\DataHelix_QUEUE\PROCESSING";

        [Verb("convert", HelpText = "Convert file formats.")]
        private class ConvertOptions
        {
            //normal options here
            [Option('t', "converter_type", Required = true, HelpText = "Converter types: xlsx2pdf, xls2xlsx, ppt2pptx, ppt2pdf, doc2pdf, doc2docx, bookmarkedDocx2pdf")]
            public string ConvertType { get; set; }
            [Option('a', "app_name", Required = true, HelpText = "App name")]
            public string AppName { get; set; }
            [Option('j', "job_id", Required = true, HelpText = "Job Id")]
            public string JobId { get; set; }
            [Option('c', "cluster_id", Required = true, HelpText = "Cluster Id")]
            public string ClusterId { get; set; }

        }
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<ConvertOptions>(args)
                .WithParsed<ConvertOptions>(opts => RunOptionsAndReturnExitCode(opts));
        }
        private static int RunOptionsAndReturnExitCode(ConvertOptions opts)
        {
            switch(opts.ConvertType)
            {
                case "ppt2pptx":
                    {
                        ConvertPpt2Pptx(opts.AppName, opts.JobId, opts.ClusterId);
                        break;
                    }
                case "ppt2pdf":
                    {
                        ConvertPpt2Pdf(opts.AppName, opts.JobId, opts.ClusterId);
                        break;
                    }
                case "xls2xlsx":
                    {
                        ConvertXLS2XLSX(opts.AppName, opts.JobId, opts.ClusterId);
                        break;
                    }
                case "doc2pdf":
                    {
                        ConvertDoc2Pdf(opts.AppName, opts.JobId, opts.ClusterId);
                        break;
                    }
                case "doc2docx":
                    {
                        ConvertDoc2Docx(opts.AppName, opts.JobId, opts.ClusterId);
                        break;
                    }
                case "xlsx2pdf":
                    {
                        ConvertXlsX2Pdf(opts.AppName, opts.JobId, opts.ClusterId);
                        break;
                    }
                case "bookmarkedDocx2pdf":
                    {
                        ConvertBookmarkedDocx2Pdf(opts.AppName, opts.JobId, opts.ClusterId);
                        break;
                    }
                default:
                    {
                        logger.Info("Entered converter type is not found.");
                        break;
                    }
            }
            return 0;
        }
        public static void ConvertPpt2Pptx(string appName, string jobId, string clusterId)
        {
            //var appName = "NFX";
            //var jobId = "00058";
            //var clusterId = "12239";
            FileFormatConverter.ConvertPPT2Pptx_Batch(
                Path.Combine(queuePath, appName, jobId, clusterId)
                , Path.Combine(queueProcessingPath, appName, jobId, clusterId, "ppt2pptx"));
        }
        public static void ConvertPpt2Pdf(string appName, string jobId, string clusterId)
        {
            //var appName = "NFX";
            //var jobId = "00058";
            //var clusterId = "12239";
            FileFormatConverter.ConvertPPT2Pdf_Batch(
                Path.Combine(queuePath, appName, jobId, clusterId)
                , Path.Combine(queueProcessingPath, appName, jobId, clusterId, "other2pdf"));
        }
        public static void ConvertXLS2XLSX(string appName, string jobId, string clusterId)
        {
            //var appName = "NFX";
            //var jobId = "00058";
            //var clusterId = "13700";
            FileFormatConverter.ConvertXls2Xlsx_Batch(
                Path.Combine(queuePath, appName, jobId, clusterId)
                , Path.Combine(queueProcessingPath, appName, jobId, clusterId, "xls2xlsx"));
            //Convert(Path.Combine(@"\\maia\h$\DataHelix_QUEUE\TO_TRAIN", clusterId), Path.Combine(@"\\maia\h$\DataHelix_QUEUE\PROCESSING\TO_TRAIN", clusterId, "xls2xlsx"));
            //ExcelTools.ConvertXLS2XLSX(@"\\maia\h$\DataHelix_QUEUE\PROCESSING\NFX\00025\122\csv", @"\\maia\h$\DataHelix_QUEUE\PROCESSING\NFX\00025\122\xls2xlsx");
        }

        public static void ConvertDoc2Pdf(string appName, string jobId, string clusterId)
        {
            //var appName = "NFX";
            //var jobId = "00058";
            //var clusterId = "9019";
            FileFormatConverter.ConvertDoc2Pdf_Batch(Path.Combine(queuePath, appName, jobId, clusterId),
                Path.Combine(queueProcessingPath, appName, jobId, clusterId, "doc2pdf"));
        }

        public static void ConvertBookmarkedDocx2Pdf(string appName, string jobId, string clusterId)
        {
            //var appName = "UL";
            //var jobId = "00004";
            //var clusterId = "2286";
            FileFormatConverter.ConvertDoc2Pdf_Batch(Path.Combine(queueProcessingPath, appName, jobId, clusterId, "bookmarkDocs"),
                Path.Combine(queueProcessingPath, appName, jobId, clusterId, "bookmarkDoc2pdf"));
        }

        public static void ConvertDoc2Docx(string appName, string jobId, string clusterId)
        {
            //var appName = "UL";
            //var jobId = "00004";
            //var clusterId = "2286";
            FileFormatConverter.ConvertDoc2Docx_Batch(Path.Combine(queuePath, appName, jobId, clusterId),
                Path.Combine(queueProcessingPath, appName, jobId, clusterId, "doc2docx"));
        }

        public static void ConvertXlsX2Pdf(string appName, string jobId, string clusterId)
        {
            //var appName = "NFX";
            //var jobId = "00057";
            //var clusterId = "5455";
            FileFormatConverter.ConvertXlsX2Pdf_Batch(Path.Combine(queuePath, appName, jobId, clusterId),
                Path.Combine(queueProcessingPath, appName, jobId, clusterId, "other2pdf"), 2);
        }
    }
}
